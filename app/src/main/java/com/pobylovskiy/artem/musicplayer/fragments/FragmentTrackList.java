package com.pobylovskiy.artem.musicplayer.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.pobylovskiy.artem.musicplayer.Constants;
import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.adapters.artist.SimpleCursorAdapterForTrack;
import com.pobylovskiy.artem.musicplayer.adapters.track.AdapterTrack;
import com.pobylovskiy.artem.musicplayer.database.DBManager;
import com.pobylovskiy.artem.musicplayer.database.MyCursorLoader;

import java.util.ArrayList;


public class FragmentTrackList extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String KEY_POSITION = "position";

    private SimpleCursorAdapterForTrack scAdapter;
    DBManager dbManager;
    private RecyclerView rv;
    private AdapterTrack adapter;

    public static FragmentTrackList newInstance(int position) {
        FragmentTrackList fragment = new FragmentTrackList();
        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        fragment.setArguments(args);
        return (fragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View result = inflater.inflate(R.layout.fragment_track, container, false);

        rv = (RecyclerView) result.findViewById(R.id.recycler_track);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        if(!getRetainInstance()) {




            dbManager = DBManager.INSTANCE.setContext(getActivity());
            getLoaderManager().initLoader(0, null, this);
           //scAdapter = new SimpleCursorAdapterForTrack(getActivity() , R.layout.list_item_track, null, from, to, 0);


        }
        setRetainInstance(true);

        return (result);
    }

    @Override
    public void onStart() {
        super.onStart();




    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), dbManager, Constants.TypeForSearchInMediaStore.TRACK_DATA);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter = new AdapterTrack(rv);
        rv.setAdapter(adapter);
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

  /*  @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Cursor c = ((SimpleCursorAdapter)l.getAdapter()).getCursor();
        Intent intent = new Intent();
        ArrayList<String> allName = new ArrayList<>();
        ArrayList<String> allAlbum = new ArrayList<>();
        ArrayList<String> allDuration = new ArrayList<>();

        intent.putExtra(Constants.ActivityPlayerArguments.POSITION, position);

        for (int i = 0; i < getListView().getCount(); i++) {
            c.moveToPosition(i);
            allName.add(c.getString(c.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
            allAlbum.add( dbManager.getAlbumArt(c.getString(c.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))));
            allDuration.add(c.getString(c.getColumnIndex(MediaStore.Audio.Media.DURATION)));
            Log.d("myLog",c.getString(c.getColumnIndex(MediaStore.Audio.Media.DURATION)));
        }

        intent.putStringArrayListExtra(Constants.ActivityPlayerArguments.TRACK_NAME, allName);
        intent.putStringArrayListExtra(Constants.ActivityPlayerArguments.ALBUM_COVER, allAlbum);
        intent.putStringArrayListExtra(Constants.ActivityPlayerArguments.TRACK_LENGTH, allDuration);

        intent.setClass(getActivity(), ActivityPlayer.class);
        startActivity(intent);

    }*/

    @Override
    public void onStop() {
        super.onStop();

    }


}
