package com.pobylovskiy.artem.musicplayer.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;
import com.pobylovskiy.artem.musicplayer.R;


public class CustomTextView extends TextView {

    public static Typeface FONT_NAME;

    public CustomTextView(Context context) {
        super(context);
        setFonts(context);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setFonts(context);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setFonts(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        setFonts(context);
    }

    private void setFonts(Context context) {
        FONT_NAME = Typeface.createFromAsset(context.getAssets(), "fonts/IntroRustBook-Base.otf");
        this.setTypeface(FONT_NAME);
      //  this.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_dark));

    }
}
