package com.pobylovskiy.artem.musicplayer.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.SimpleCursorAdapter;

import com.pobylovskiy.artem.musicplayer.Constants;
import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.adapters.album.AdapterAlbum;
import com.pobylovskiy.artem.musicplayer.adapters.track.AdapterTrack;
import com.pobylovskiy.artem.musicplayer.database.DBManager;
import com.pobylovskiy.artem.musicplayer.database.MyCursorLoader;

import java.util.ArrayList;

public class FragmentAlbumList extends Fragment implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor>{

    public static final String KEY_POSITION = "position";

    private int position;
    private FrameLayout stuffContainer;
    private GridView albumGrid;
    private FragmentManager fragmentManager;
    private  RecyclerView rv;

    private AdapterAlbum adapter;
    DBManager dbManager;

    public static FragmentAlbumList newInstance(int position) {
        FragmentAlbumList fragment = new FragmentAlbumList();
        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        fragment.setArguments(args);
        return (fragment);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View result = inflater.inflate(R.layout.fragment_album, container, false);
        stuffContainer = (FrameLayout) result.findViewById(R.id.stuff_container);
        //albumGrid = (GridView) result.findViewById(R.id.album_grid);

        position = getArguments().getInt(KEY_POSITION);

        rv = (RecyclerView) result.findViewById(R.id.recycler_track);
       // rv.addItemDecoration(new MarginDecoration(this));
        dbManager = DBManager.INSTANCE.setContext(getActivity());
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        getLoaderManager().initLoader(0, null, this);
        if (!getRetainInstance()) {


        /*    String[] from = new String[] {
                    MediaStore.Audio.AlbumColumns.ALBUM_ART,
                    MediaStore.Audio.AlbumColumns.ALBUM,
                    MediaStore.Audio.AlbumColumns.NUMBER_OF_SONGS};

            int[] to = new int[] {
                    R.id.item_icon_album,
                    R.id.item_album_name,
                    R.id.item_quantity_tracks};

            dbManager = DBManager.INSTANCE.setContext(getActivity());

            scAdapter = new SimpleCursorAdapterForAlbum(getActivity() , R.layout.list_item_album, null, from, to, 0);

            albumGrid.setAdapter(scAdapter);

        */



        }
        setRetainInstance(true);
        return (result);
    }

    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), dbManager, Constants.TypeForSearchInMediaStore.ALBUM_DATA);
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {
        adapter = new AdapterAlbum(rv, getFragmentManager());
        rv.setAdapter(adapter);

        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {

    }

    @Override
    public void onStop() {
        super.onStop();
//        albumGrid.removeAllViews();
    }

    @Override
    public void onStart() {
        super.onStart();


      /*  albumGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cursor c = ((SimpleCursorAdapter) albumGrid.getAdapter()).getCursor();
                c.moveToPosition(position);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.FlagsForDetailsItem.ID, c.getString(c.getColumnIndex(
                        MediaStore.Audio.Albums._ID)));
                bundle.putString(Constants.FlagsForDetailsItem.WHERE ,MediaStore.Audio.Media.ALBUM_ID);
                bundle.putString(Constants.FlagsForDetailsItem.PLAYLIST_NAME ,c.getString(c.getColumnIndex(
                        MediaStore.Audio.Albums.ALBUM)));

                //bundle.putString(Constants.FlagsForDetailsItem.TYPE, String.valueOf(position));


                new BuilderFragmentTransaction()
                        .fragmentManager(getFragmentManager())
                        .fragment(new FragmentTrack())
                        .bundle(bundle)
                        .transact(R.id.container_for_track, true);
                    }
        });*/

    }

    @Override
    public void onPause() {
        super.onPause();
        //  albumGrid.removeAllViews();
    }
}