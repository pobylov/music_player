package com.pobylovskiy.artem.musicplayer.model.track;

/**
 * Created by Artem on 02.01.2016.
 */
public interface Mode {

    public static final int PLAY=1;
    public static final int PAUSE=2;
    public static final int GONE=3;
}
