package com.pobylovskiy.artem.musicplayer.adapters.album;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pobylovskiy.artem.musicplayer.BitmapActions;
import com.pobylovskiy.artem.musicplayer.Constants;
import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.ScreenSizeProvider;
import com.pobylovskiy.artem.musicplayer.activitys.ActivityPlayer;
import com.pobylovskiy.artem.musicplayer.activitys.ActivityTrackList;
import com.pobylovskiy.artem.musicplayer.fragments.BuilderFragmentTransaction;
import com.pobylovskiy.artem.musicplayer.fragments.FragmentTrack;
import com.pobylovskiy.artem.musicplayer.model.ListAttributeForBundle;
import com.pobylovskiy.artem.musicplayer.model.album.Album;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AdapterAlbum extends RecyclerView.Adapter<AdapterAlbum.AlbumViewHolder> {

    RecyclerView rv;
    private Cursor cursor;
    private Context context;
    private int bitmapSize;
    private FragmentManager fm;

    public static class AlbumViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView quantity;
        ImageView cover;


        AlbumViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.item_album_name);
            quantity = (TextView) itemView.findViewById(R.id.item_quantity_tracks);
            cover = (ImageView) itemView.findViewById(R.id.item_icon_album);
        }
    }

    List<Album> albums;

    public AdapterAlbum(RecyclerView rv, FragmentManager fm) {
        this.rv = rv;
        context = rv.getContext();
        this.fm = fm;

         bitmapSize = (int) (ScreenSizeProvider.getScreenWidth((Activity) context) / 2.2f);
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_album, viewGroup, false);

        AlbumViewHolder pvh = new AlbumViewHolder(v);
        return pvh;
    }


    @Override
    public void onBindViewHolder(final AlbumViewHolder holder, final int i) {
        holder.name.setText(albums.get(i).getName());
        holder.quantity.setText(albums.get(i).getQuantity());
       // holder.cover.setImageResource();
        try {
            Picasso.with(context)
                    .load(new File(albums.get(i).getCover()))
                            .placeholder(bitmapCorrected(R.drawable.logo_big))
                    .error(R.drawable.logo_big)
                            //.transform(new CircleImage())
                            .resize(bitmapSize, bitmapSize)
                    .into(holder.cover);
        }catch (Exception e){
            Picasso.with(context)
                    .load(R.drawable.logo_big)
                    .error(R.drawable.logo_big)
                    .resize(bitmapSize, bitmapSize)
                    .into(holder.cover);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v){

                                                   cursor.moveToPosition(i);
                                                   ListAttributeForBundle attributeForBundle = new ListAttributeForBundle(
                                                          cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums._ID)),
                                                          MediaStore.Audio.Media.ALBUM_ID,
                                                          cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM))
                                                   );
                                                   Intent intent = new Intent();
                                                   intent.putExtra(Constants.TypeForDetailsItem.ALBUM_DETAILS, attributeForBundle);
                                                   intent.setClass((Activity) v.getContext(), ActivityTrackList.class);
                                                   ((Activity) v.getContext()).startActivity(intent);

                                               }
                                           }
        );
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        // Log.d("-----------------", String.valueOf(persons.size()));
        return albums.size();
    }

    public void swapCursor(Cursor cursor) {
        this.cursor = cursor;
        albums = getData(cursor);
    }

    private ArrayList<Album> getData(Cursor cursor) {
        ArrayList<Album> dataContainer = new ArrayList<>();

        String[] from = new String[] {
                MediaStore.Audio.AlbumColumns.ALBUM,
                MediaStore.Audio.AlbumColumns.NUMBER_OF_SONGS,
                MediaStore.Audio.AlbumColumns.ALBUM_ART};

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    dataContainer.add(new Album(
                            cursor.getString(cursor.getColumnIndex(from[0])),
                            cursor.getString(cursor.getColumnIndex(from[1])),
                            cursor.getString(cursor.getColumnIndex(from[2]))));


                } while (cursor.moveToNext());

            }
        }
        //cursor.close();


        return dataContainer;

    }

    private Drawable bitmapCorrected(int ref){
        Drawable drawable =  context.getResources().getDrawable(ref);
        Bitmap bitmap = BitmapActions.drawableToBitmap(drawable);
        bitmap = BitmapActions.bitmapResize(bitmap, (int) (ScreenSizeProvider.getScreenWidth((Activity) context)/1.5),
                (int) (ScreenSizeProvider.getScreenWidth((Activity)context)/1.5));

        return BitmapActions.bitmapToDrawable(bitmap);
    }

    private String timeConversion(String miliSec) {


        long miliSeconds = Long.valueOf(miliSec);
        int MINUTES_IN_AN_HOUR = 60;
        int SECONDS_IN_A_MINUTE = 60;
        int MILISECONDS_IN_A_SECONDS = 1000;

        long seconds = miliSeconds / MILISECONDS_IN_A_SECONDS;

        long minutes = seconds / SECONDS_IN_A_MINUTE;
        seconds -= minutes * SECONDS_IN_A_MINUTE;

        long hours = minutes / MINUTES_IN_AN_HOUR;
        minutes -= hours * MINUTES_IN_AN_HOUR;

        if(seconds-10<0) {

            if (hours != 0) {
                return hours + ":" + minutes + ":" + "0"+seconds;
            } else {
                return minutes + ":" + "0"+seconds;
            }
        }
        else {

            if (hours != 0) {
                return hours + ":" + minutes + ":" + seconds;
            } else {
                return minutes + ":" + seconds;
            }
        }

    }
}

