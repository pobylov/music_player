package com.pobylovskiy.artem.musicplayer.view_pager;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.fragments.FragmentAlbumList;
import com.pobylovskiy.artem.musicplayer.fragments.FragmentArtistList;
import com.pobylovskiy.artem.musicplayer.fragments.FragmentPlayList;
import com.pobylovskiy.artem.musicplayer.fragments.FragmentTrackList;

/**
 * Created by Artem on 01.07.2015.
 */
public class SampleAdapter extends FragmentPagerAdapter {

    private Context context;
    private String[] titleList = new String[]{"Album","PlayList","Tracks","Artist"};


    private ActionBar actionBar;

    public SampleAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        Activity activity = (Activity) context;
        actionBar = activity.getActionBar();

    }

    @Override
    public Fragment getItem(int position) {
        switch (position){

            case 0:
                return FragmentAlbumList.newInstance(position);

            case 1:
                return FragmentPlayList.newInstance(position);

            case 2:
                return FragmentTrackList.newInstance(position);

            case 3:
                return FragmentArtistList.newInstance(position);
        }

        return null;
    }




    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){

            case 0:
                return titleList[0];

            case 1:
                return titleList[1];

            case 2:
                return titleList[2];

            case 3:
                return titleList[3];
        }



        return null;
    }
}
