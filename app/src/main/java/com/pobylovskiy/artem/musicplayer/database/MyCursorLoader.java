package com.pobylovskiy.artem.musicplayer.database;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import com.pobylovskiy.artem.musicplayer.Constants;

/**
 * Created by Artem on 04.09.2015.
 */
public class MyCursorLoader extends CursorLoader {

    DBManager db;
    String typeForSearch;
    String id;
    String where;

    public MyCursorLoader(Context context, DBManager db, String typeForSearch) {
        super(context);
        this.db = db;
        this.typeForSearch = typeForSearch;
    }

    public MyCursorLoader(Context context, DBManager db, String id, String where ,String typeForSearch) {
        super(context);
        this.db = db;
        this.typeForSearch = typeForSearch;
        this.id = id;
        this.where = where;
    }

    @Override
    public Cursor loadInBackground() {
        Cursor cursor;
        switch (typeForSearch){

            case Constants.TypeForSearchInMediaStore.ARTIST_DATA:
                cursor = db.getAllDataArtist();
                return cursor;

            case Constants.TypeForSearchInMediaStore.PLAYLIST_DATA:
                cursor = db.getAllDataPlayList();
                return cursor;

            case Constants.TypeForSearchInMediaStore.TRACK_DATA:
                cursor = db.getAllDataTrack();
                return cursor;

            case Constants.TypeForSearchInMediaStore.ALBUM_DATA:
                cursor = db.getAllDataAlbum();
                return cursor;

            case Constants.FlagsForDetailsItem.DETAILS:
                cursor = db.getTracks(id, where);
                return cursor;
        }


        return null;
    }

}
