package com.pobylovskiy.artem.musicplayer.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Artem on 26.01.2016.
 */
public class ListAttributeForBundle implements Parcelable {

    private String id;
    private String where;
    private String name;

    public ListAttributeForBundle(String id, String where, String name) {
        this.id = id;
        this.where = where;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.where);
        dest.writeString(this.name);
    }

    private ListAttributeForBundle(Parcel in) {
        this.id = in.readString();
        this.where = in.readString();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<ListAttributeForBundle> CREATOR = new Parcelable.Creator<ListAttributeForBundle>() {
        public ListAttributeForBundle createFromParcel(Parcel source) {
            return new ListAttributeForBundle(source);
        }

        public ListAttributeForBundle[] newArray(int size) {
            return new ListAttributeForBundle[size];
        }
    };
}
