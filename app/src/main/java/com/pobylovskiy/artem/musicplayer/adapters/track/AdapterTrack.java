package com.pobylovskiy.artem.musicplayer.adapters.track;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pobylovskiy.artem.musicplayer.Constants;
import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.custom.ImageViewMode;
import com.pobylovskiy.artem.musicplayer.database.DBManager;
import com.pobylovskiy.artem.musicplayer.activitys.ActivityPlayer;
import com.pobylovskiy.artem.musicplayer.model.track.Mode;
import com.pobylovskiy.artem.musicplayer.model.track.Track;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artem on 27.11.2015.
 */
public class AdapterTrack extends RecyclerView.Adapter<AdapterTrack.TrackViewHolder> {

    RecyclerView rv;
    private Cursor cursor;

    public static class TrackViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView artist;
        TextView length;
        ImageViewMode trackMode;


        TrackViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.item_track_name);
            artist = (TextView) itemView.findViewById(R.id.item_track_artist_name);
            length = (TextView) itemView.findViewById(R.id.item_track_length);
            trackMode = (ImageViewMode) itemView.findViewById(R.id.track_mode);
        }
    }

    List<Track> tracks;

    public AdapterTrack(RecyclerView rv) {
        this.rv = rv;
    }

    @Override
    public TrackViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_track, viewGroup, false);
        TrackViewHolder pvh = new TrackViewHolder(v);
        return pvh;
    }


    @Override
    public void onBindViewHolder(final TrackViewHolder holder, final int i) {
        holder.name.setText(tracks.get(i).getName());
        holder.artist.setText(tracks.get(i).getArtist());
        holder.length.setText(timeConversion(tracks.get(i).getLength()));
        holder.trackMode.setMode(tracks.get(i).getMode());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v){

                                                   Intent intent = new Intent();
                                                   Track track = tracks.get(i);
                                                   DBManager dbManager = DBManager.INSTANCE;
                                                   cursor.moveToPosition(i);
                                                   track.setAlbumId(dbManager.getAlbumArt(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))));
                                                   track.setMode(Mode.PLAY);

                                                   holder.itemView.invalidate();
                                                   //rv.getLayoutManager().onItemsUpdated(rv, i, 1);
                                                    intent.putParcelableArrayListExtra(Constants.ActivityPlayerArguments.TRACK_LIST, (ArrayList<? extends android.os.Parcelable>) tracks);
                                                   intent.putExtra(Constants.ActivityPlayerArguments.TRACK, track);
                                                   intent.setClass((Activity) v.getContext(), ActivityPlayer.class);
                                                   ((Activity) v.getContext()).startActivity(intent);
                                                   //

                                               }
                                           }
        );
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        // Log.d("-----------------", String.valueOf(persons.size()));
        return tracks.size();
    }

    public void swapCursor(Cursor cursor) {
        this.cursor = cursor;
        tracks = getData(cursor);
    }

    private ArrayList<Track> getData(Cursor cursor) {
        ArrayList<Track> dataContainer = new ArrayList<Track>();
        String[] from = new String[]{
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DURATION};

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    dataContainer.add(new Track(
                            cursor.getString(cursor.getColumnIndex(from[0])),
                            cursor.getString(cursor.getColumnIndex(from[1])),
                            cursor.getString(cursor.getColumnIndex(from[2])), Mode.GONE));


                } while (cursor.moveToNext());

            }
        }
        //cursor.close();


        return dataContainer;

    }

    private String timeConversion(String miliSec) {


        long miliSeconds = Long.valueOf(miliSec);
        int MINUTES_IN_AN_HOUR = 60;
        int SECONDS_IN_A_MINUTE = 60;
        int MILISECONDS_IN_A_SECONDS = 1000;

        long seconds = miliSeconds / MILISECONDS_IN_A_SECONDS;

        long minutes = seconds / SECONDS_IN_A_MINUTE;
        seconds -= minutes * SECONDS_IN_A_MINUTE;

        long hours = minutes / MINUTES_IN_AN_HOUR;
        minutes -= hours * MINUTES_IN_AN_HOUR;

        if(seconds-10<0) {

            if (hours != 0) {
                return hours + ":" + minutes + ":" + "0"+seconds;
            } else {
                return minutes + ":" + "0"+seconds;
            }
        }
        else {

            if (hours != 0) {
                return hours + ":" + minutes + ":" + seconds;
            } else {
                return minutes + ":" + seconds;
            }
        }

    }
}

