package com.pobylovskiy.artem.musicplayer.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.provider.MediaStore;

import com.pobylovskiy.artem.musicplayer.Constants;


/**
 * Created by Artem on 04.09.2015.
 */
public class DBManager  implements BaseColumns {

    private Context context;
    private SQLiteDatabase sqLiteDatabase;
    public static final DBManager INSTANCE = new DBManager();

    public Cursor getAllDataArtist() {
        String[] columns = {MediaStore.Audio.Artists._ID,
                MediaStore.Audio.ArtistColumns.ARTIST,
                MediaStore.Audio.ArtistColumns.NUMBER_OF_TRACKS,
                MediaStore.Audio.ArtistColumns.ARTIST_KEY};
        Cursor cursor = context.getContentResolver().query(MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI, columns, MediaStore.Audio.Artists._ID, null, null);

        return cursor;
    }

    public Cursor getAllDataTrack() {
        String[] columns =  { MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM_ID};
        Cursor cursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, columns, MediaStore.Audio.Media._ID, null, null);

        return cursor;
    }

    public Cursor getAllDataPlayList() {
        String[] columns = {MediaStore.Audio.Playlists._ID,
                MediaStore.Audio.PlaylistsColumns.NAME};
        Cursor cursor = context.getContentResolver().query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, columns, MediaStore.Audio.Playlists._ID, null, null);

        return cursor;
    }
    public Cursor getAllDataAlbum() {
        String[] columns = {MediaStore.Audio.Albums._ID,
                MediaStore.Audio.AlbumColumns.ALBUM_ART,
                MediaStore.Audio.AlbumColumns.ALBUM,
                MediaStore.Audio.AlbumColumns.NUMBER_OF_SONGS
        };

        Cursor cursor = context.getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, columns, MediaStore.Audio.Albums._ID, null, null);

        return cursor;
    }

    public String getAlbumArt(String albumID){
        String[] columns = {MediaStore.Audio.Albums._ID,
                MediaStore.Audio.AlbumColumns.ALBUM_ART,};

        Cursor cursor = context.getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, columns, MediaStore.Audio.Albums._ID + " = " + albumID, null, null);

        return getData(cursor, MediaStore.Audio.AlbumColumns.ALBUM_ART);
    }

    public Cursor getTracks(String search, String where){
        String[] column = {"*"};

        where =where  + "=?";

        String whereVal[] = {search};

        String orderBy = android.provider.MediaStore.Audio.Media.TITLE;

        Cursor cursor;
        if(where.equals(MediaStore.Audio.Playlists.Members.PLAYLIST_ID + "=?")) {
            cursor = context.getContentResolver().query(MediaStore.Audio.Playlists.Members.getContentUri("external", Long.valueOf(search)), column, where, whereVal, null);
        }else {
            cursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, column, where, whereVal, orderBy);

        }

        return cursor;
    }


    public void delRec(long id) {
       // sqLiteDatabase.delete(DATABASE_TABLE, BaseColumns._ID + " = " + id, null);
    }

     protected String getData(Cursor cursor, String columnsForSearch) {
       String dataContainer = "";
        if (cursor != null && columnsForSearch != null) {
            if (cursor.moveToFirst()) {
                do {

                    dataContainer = cursor.getString(cursor.getColumnIndex(columnsForSearch));

                } while (cursor.moveToNext());

            }
             cursor.close();
        }

        return dataContainer;
    }


    public DBManager setContext(Context context) {
        this.context = context;
        return this;
    }
}
