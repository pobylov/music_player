package com.pobylovskiy.artem.musicplayer.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;

import com.pobylovskiy.artem.musicplayer.Constants;
import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.fragments.BuilderFragmentTransaction;
import com.pobylovskiy.artem.musicplayer.fragments.FragmentTrack;

/**
 * Created by Artem on 26.01.2016.
 */
public class ActivityTrackList extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_list);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.TypeForDetailsItem.ALBUM_DETAILS, getIntent().getParcelableExtra(Constants.TypeForDetailsItem.ALBUM_DETAILS));
        new BuilderFragmentTransaction().fragment(new FragmentTrack()).fragmentManager(getSupportFragmentManager()).bundle(bundle).transact(R.id.rl_container, false);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setLogo(getResources().getDrawable(R.drawable.logo));


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}
