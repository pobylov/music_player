package com.pobylovskiy.artem.musicplayer.record_view.surface;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.PointF;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class CircleProgressSurface extends SurfaceView implements SurfaceHolder.Callback, ThreadStatus, View.OnTouchListener {

    private DrawThreadProgress drawThread;
    private Context context;
    private Bitmap cover;
    private long trackLength = 0;
    private GestureDetector gestureDetector;

    public CircleProgressSurface(Context context) {
        super(context);
        this.context = context;
        holder();
    }

    public CircleProgressSurface(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        holder();
    }

    public CircleProgressSurface(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        holder();
    }

    private void holder() {

        setZOrderOnTop(true);
        getHolder().addCallback(this);
        getHolder().setFormat(PixelFormat.TRANSLUCENT);
        setOnTouchListener(this);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {

        //Log.d("SURFACEChanged", String.valueOf(width)+" "+ String.valueOf(height));
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        onStop();

    }

    public void start(){
        drawThread = new DrawThreadProgress(getHolder(), context, this.getMeasuredWidth(), this.getMeasuredHeight());
        //Log.d("SURFACE", String.valueOf(this.getMeasuredWidth())+" "+ String.valueOf( this.getMeasuredHeight()));
        drawThread.setTrackLength(trackLength);
        drawThread.setCover(cover);
        drawThread.onStart();
    }

    public void setCircleImage(Bitmap cover) {
        this.cover = cover;
        //drawThread.setCover(cover);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setTrackLength(long trackLength) {
        this.trackLength = trackLength;
//        drawThread.setTrackLength(trackLength);
    }


    @Override
    public void onPause() {
        drawThread.onPause();
    }

    @Override
    public void onResume() {
        drawThread.onResume();
    }

    @Override
    public void onStop() {
        drawThread.onStop();
    }

    @Override
    public void onStart() {
        drawThread.onStart();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        PointF curr = new PointF(event.getX(), event.getY());
        gestureDetector = initGestureDetector();
        /*switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_UP:
                drawThread.setSleepTime(1000);
                break;
        }*/
        return gestureDetector.onTouchEvent(event);
    }

    private GestureDetector initGestureDetector() {
        return new GestureDetector(new GestureDetector.SimpleOnGestureListener() {

            private Gesture detector = new Gesture();

            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                   float velocityY) {
                try {
                    if (detector.isDragDown(e1, e2, velocityX)) {
                        drawThread.setSleepTime((int) (100));
                        return false;
                    } else if (detector.isDragUp(e1, e2, velocityX)) {
                        drawThread.setSleepTime((int) (100));
                    }/*else if (detector.isSwipeLeft(e1, e2, velocityX)) {
                        showToast("Left Swipe");
                    } else if (detector.isSwipeRight(e1, e2, velocityX)) {
                        showToast("Right Swipe");
                    }*/
                } catch (Exception e) {} //for now, ignore
                return false;
            }

        });
    }
}
