package com.pobylovskiy.artem.musicplayer.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.model.track.Mode;

/**
 * Created by Artem on 02.01.2016.
 */
public class ImageViewMode extends ImageView {

    public ImageViewMode(Context context) {
        super(context);
    }

    public ImageViewMode(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewMode(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ImageViewMode(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    public void setMode(int trackMode){
        switch (trackMode){
            case Mode.GONE:
                this.setVisibility(GONE);
                break;
            case Mode.PLAY:
                this.setVisibility(VISIBLE);
                this.setImageResource(R.drawable.ic_resume_mini);
                break;
            case Mode.PAUSE:
                this.setVisibility(VISIBLE);
                this.setImageResource(R.drawable.ic_pause_mini);
                break;
        }

    }
}
