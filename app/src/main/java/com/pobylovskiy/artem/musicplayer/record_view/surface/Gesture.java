package com.pobylovskiy.artem.musicplayer.record_view.surface;

import android.view.MotionEvent;

/**
 * Created by Artem on 24.09.2015.
 */
public class Gesture {

    private int swipe_distance;
    private int swipe_velocity;
    private static final int SWIPE_MIN_DISTANCE = 20;
    private static final int SWIPE_THRESHOLD_VELOCITY = 10;

    public Gesture(int distance, int velocity) {
        super();
        this.swipe_distance = distance;
        this.swipe_velocity = velocity;
    }

    public Gesture() {
        super();
        this.swipe_distance = SWIPE_MIN_DISTANCE;
        this.swipe_velocity = SWIPE_THRESHOLD_VELOCITY;
    }

    public boolean isDragDown(MotionEvent e1, MotionEvent e2, float velocityY) {
        return isDrag(e2.getY(), e1.getY(), velocityY);
    }

    public boolean isDragUp(MotionEvent e1, MotionEvent e2, float velocityY) {
        return isDrag(e1.getY(), e2.getY(), velocityY);
    }

   /* public boolean isSwipeLeft(MotionEvent e1, MotionEvent e2, float velocityX) {
        return isSwipe(e1.getX(), e2.getX(), velocityX);
    }

    public boolean isSwipeRight(MotionEvent e1, MotionEvent e2, float velocityX) {
        return isSwipe(e2.getX(), e1.getX(), velocityX);
    }*/

    private boolean isSwipeDistance(float coordinateA, float coordinateB) {
        return (coordinateA - coordinateB) > this.swipe_distance;
    }

    private boolean isDragSpeed(float velocity) {
        return Math.abs(velocity) > this.swipe_velocity;
    }

    private boolean isDrag(float coordinateA, float coordinateB, float velocity) {
        return isDragSpeed(velocity);
    }
}
