package com.pobylovskiy.artem.musicplayer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridView;

import com.pobylovskiy.artem.musicplayer.R;

/**
 * Created by Artem on 01.01.2016.
 */
public class FragmentBoot extends Fragment {

    public static FragmentBoot newInstance() {
        FragmentBoot fragment = new FragmentBoot();
        return (fragment);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View result = inflater.inflate(R.layout.fragment_boot, container, false);

        return result;
    }
}