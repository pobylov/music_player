package com.pobylovskiy.artem.musicplayer.custom;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;


public class ImageViewSelector extends ImageView implements View.OnTouchListener {

    private ColorStateList tint;
    private Context context;
    private ScaleAnimation animation;
    private Vibrator vib;

    public ImageViewSelector(Context context) {
        super(context);
        this.context = context;
        setOnTouchListener(this);
        vibrate(context);
    }

    public ImageViewSelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setOnTouchListener(this);
        vibrate(context);
    }

    public ImageViewSelector(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        setOnTouchListener(this);
        vibrate(context);
    }


    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (tint != null && tint.isStateful())
            updateTintColor();
    }

    private void updateTintColor() {
        int color = tint.getColorForState(getDrawableState(), 0);
        setColorFilter(color);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                //setColorFilter(context.getResources().getColor(R.color.primary), PorterDuff.Mode.MULTIPLY);
                ScaleView(v);
                vib.vibrate(15);
                break;
            case MotionEvent.ACTION_UP:

                //setColorFilter(context.getResources().getColor(R.color.button_player_color), PorterDuff.Mode.MULTIPLY);

        }


        return false;
    }

    public void ScaleView(View v) {
         animation = new ScaleAnimation(1f, 0.8f, 1f, 0.8f, v.getWidth() / 2, v.getHeight() / 2);
        animation.setDuration(200);
        animation.setFillAfter(false);
        v.startAnimation(animation);

    }

    private void vibrate(Context context) {
        vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }
}




