package com.pobylovskiy.artem.musicplayer.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.pobylovskiy.artem.musicplayer.Constants;
import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.database.DBManager;
import com.pobylovskiy.artem.musicplayer.database.MyCursorLoader;

public class FragmentArtistList extends ListFragment implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor> {

    public static final String KEY_POSITION = "position";
    private SimpleCursorAdapter scAdapter;
    DBManager dbManager;


    public static FragmentArtistList newInstance(int position) {
        FragmentArtistList fragment = new FragmentArtistList();
        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        fragment.setArguments(args);
        return (fragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View result = inflater.inflate(R.layout.fragment_artist, container, false);

        if(!getRetainInstance()) {
        String[] from = new String[] {MediaStore.Audio.ArtistColumns.ARTIST, MediaStore.Audio.ArtistColumns.NUMBER_OF_TRACKS};
            int[] to = new int[] { R.id.item_artist_name, R.id.item_artist_quantity_tracks};

            dbManager = DBManager.INSTANCE.setContext(getActivity());

            scAdapter = new SimpleCursorAdapter(getActivity() , R.layout.list_item_artist, null, from, to, 0);

            setListAdapter(scAdapter);

            getLoaderManager().initLoader(0, null, this);

        }

        setRetainInstance(true);

        return (result);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Bundle bundle = new Bundle();
        Cursor c = ((SimpleCursorAdapter) l.getAdapter()).getCursor();
        c.moveToPosition(position);

        bundle.putString(Constants.FlagsForDetailsItem.ID, c.getString(c.getColumnIndex(
                MediaStore.Audio.ArtistColumns.ARTIST_KEY)));
        bundle.putString(Constants.FlagsForDetailsItem.WHERE ,MediaStore.Audio.ArtistColumns.ARTIST_KEY);

        new BuilderFragmentTransaction()
                .fragmentManager(getFragmentManager())
                .fragment(new FragmentTrack())
                .bundle(bundle)
                .transact(R.id.container_for_track, true);
    }


    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), dbManager, Constants.TypeForSearchInMediaStore.ARTIST_DATA);
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {
        scAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {

    }
}
