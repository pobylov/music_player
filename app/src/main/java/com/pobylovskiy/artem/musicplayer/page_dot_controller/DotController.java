package com.pobylovskiy.artem.musicplayer.page_dot_controller;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.view_pager.PagerTitleStrip;

import java.util.List;

public class DotController extends View implements ViewPager.OnPageChangeListener {

    Context context;
    ViewPager viewPager;
    int offIcon;
    int onIcon;
    LinearLayout linearLayout;

    private PagerTitleStrip pagerTitleStrip;

    public DotController(Context context) {
        super(context);
        this.context = context;
    }

    public DotController(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public DotController(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DotController(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec/2, heightMeasureSpec/2);
    }

    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
    }


    public void setPagerTitleStrip(PagerTitleStrip pagerTitleStrip) {
        this.pagerTitleStrip = pagerTitleStrip;
    }


    public void setOffIcon(int offIcon) {
        this.offIcon = offIcon;
    }

    public void setOnIcon(int onIcon) {
        this.onIcon = onIcon;
    }

    public ViewGroup createDotController(){
        linearLayout = createLinearLayout();
        viewPager.setOnPageChangeListener(this);
        for (int i=0; i < viewPager.getAdapter().getCount(); i++) {
                  linearLayout.addView(createImageView(String.valueOf(viewPager.getAdapter().getPageTitle(i))));
        }
        imageSwitcher(viewPager.getCurrentItem());
        titleSwitcher(viewPager.getCurrentItem());

        return linearLayout;
    }

    private void imageSwitcher(int position){

       ImageView imageView;
       ImageView currentImageView = (ImageView) linearLayout.getChildAt(position);
        for(int i=0; i < viewPager.getAdapter().getCount(); i++){

            imageView = (ImageView) linearLayout.getChildAt(i);
            imageView.setImageResource(offIcon);
        }
        currentImageView.setImageResource(onIcon);


    }

    private void titleSwitcher(int position){

        TextView currentHeader = (TextView) pagerTitleStrip.getChildAt(position+1);

        for(int i=0; i < viewPager.getAdapter().getCount()-1; i++){
         //   Log.d("+++++++++++++" , String.valueOf(i)+ " "+String.valueOf(viewPager.getAdapter().getCount()));
            TextView textView = (TextView) pagerTitleStrip.getChildAt(i);
            textView.setTextColor(context.getResources().getColor(R.color.button_player_color));

        }

        currentHeader.setTextColor(context.getResources().getColor(R.color.primary_light));



    }

    protected ImageView createImageView(String tag) {
        ImageView IV = new ImageView(context);
        int wrap_content=LinearLayout.LayoutParams.WRAP_CONTENT;
        IV.setTag(tag);
        return IV;
    }


    protected LinearLayout createLinearLayout() {
        LinearLayout LL = new LinearLayout(context);
        int wrap_content=LinearLayout.LayoutParams.WRAP_CONTENT;
        int match_parent=LinearLayout.LayoutParams.MATCH_PARENT;
        LL.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(match_parent, wrap_content);
        LL.setGravity(Gravity.CENTER);
      /*  params.weight=1.0f;
         params.gravity=Gravity.CENTER;*/
        LL.setLayoutParams(params);

        return LL;
    }


    private LinearLayout.LayoutParams params(float width, float height, float margins) {
        //int match_parent=LinearLayout.LayoutParams.MATCH_PARENT;
        //int wrap_content=LinearLayout.LayoutParams.WRAP_CONTENT;

        LinearLayout.LayoutParams params= new LinearLayout.LayoutParams((int) width, (int) height);
        params.setMargins((int) (margins+margins) , (int) margins, (int) (margins+margins), (int) margins);
        //params.weight=1.0f;
        params.gravity=Gravity.CENTER;

        return params;
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        imageSwitcher(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
