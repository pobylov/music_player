package com.pobylovskiy.artem.musicplayer.record_view.surface;


public interface ThreadStatus {

    public void onPause();
    public void onResume();
    public void onStop();
    public void onStart();
}
