package com.pobylovskiy.artem.musicplayer.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.pobylovskiy.artem.musicplayer.Constants;
import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.adapters.artist.SimpleCursorAdapterForTrack;
import com.pobylovskiy.artem.musicplayer.adapters.track.AdapterTrack;
import com.pobylovskiy.artem.musicplayer.database.DBManager;
import com.pobylovskiy.artem.musicplayer.database.MyCursorLoader;
import com.pobylovskiy.artem.musicplayer.model.ListAttributeForBundle;

import java.util.ArrayList;

public class FragmentTrack extends Fragment implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor>{


    private SimpleCursorAdapter scAdapter;
    private DBManager dbManager;
    private String type, where, name;
    private RecyclerView rv;
    private AdapterTrack adapter;
    private TextView detailsName;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View result = inflater.inflate(R.layout.fragment__item_details, container, false);
        detailsName = (TextView) result.findViewById(R.id.tv_details_name);
      /*   if(!getRetainInstance()) {


           String[] from = new String[] {
                    MediaStore.Audio.Media.DISPLAY_NAME,
                    MediaStore.Audio.Media.ARTIST,
                    MediaStore.Audio.Media.DURATION};

            int[] to = new int[] {
                    R.id.item_track_name,
                    R.id.item_track_artist_name,
                    R.id.item_track_length};

            dbManager = DBManager.INSTANCE.setContext(getActivity());

            scAdapter = new SimpleCursorAdapterForTrack(getActivity() , R.layout.list_item_track, null, from, to, 0);

            setListAdapter(scAdapter);

            getLoaderManager().initLoader(0, null, this);

        }   */

        rv = (RecyclerView) result.findViewById(R.id.recycler_track);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        if(!getRetainInstance()) {
            ListAttributeForBundle attributeForBundle = getArguments().getParcelable(Constants.TypeForDetailsItem.ALBUM_DETAILS);
            type = attributeForBundle.getId();
            where = attributeForBundle.getWhere();
            name = attributeForBundle.getName();


            detailsName.setText(name);
            dbManager = DBManager.INSTANCE.setContext(getActivity());
            getLoaderManager().initLoader(0, null, this);
            //scAdapter = new SimpleCursorAdapterForTrack(getActivity() , R.layout.list_item_track, null, from, to, 0);


        }
        setRetainInstance(true);
        return result;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), dbManager, type, where ,Constants.FlagsForDetailsItem.DETAILS);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter = new AdapterTrack(rv);
        rv.setAdapter(adapter);
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

/*    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Cursor c = ((SimpleCursorAdapter)l.getAdapter()).getCursor();
        c.moveToPosition(position);
        Intent intent = new Intent();
        ArrayList<String> allName = new ArrayList<>();
        ArrayList<String> allAlbum = new ArrayList<>();
        ArrayList<String> allDuration = new ArrayList<>();

        intent.putExtra(Constants.ActivityPlayerArguments.POSITION, position);

        for (int i = 0; i < getListView().getCount(); i++) {
            c.moveToPosition(i);
            allName.add(c.getString(c.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
            allAlbum.add( dbManager.getAlbumArt(c.getString(c.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))));
            allDuration.add(c.getString(c.getColumnIndex(MediaStore.Audio.Media.DURATION)));
            Log.d("myLog",c.getString(c.getColumnIndex(MediaStore.Audio.Media.DURATION)));
        }

        intent.putStringArrayListExtra(Constants.ActivityPlayerArguments.TRACK_NAME, allName);
        intent.putStringArrayListExtra(Constants.ActivityPlayerArguments.ALBUM_COVER, allAlbum);
        intent.putStringArrayListExtra(Constants.ActivityPlayerArguments.TRACK_LENGTH, allDuration);

        intent.setClass(getActivity(), ActivityPlayer.class);
        startActivity(intent);


    }*/
}
