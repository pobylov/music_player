package com.pobylovskiy.artem.musicplayer.view_pager;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pobylovskiy.artem.musicplayer.R;

/**
 * Created by Artem on 01.07.2015.
 */
public class PagerTitleStrip extends android.support.v4.view.PagerTitleStrip {

    private static Typeface FONT_NAME;
    private float width, height;


    public PagerTitleStrip(Context context) {
        super(context);
        initScreenSize((Activity)context);
        setFonts(context);
    }

    public PagerTitleStrip(Context context, AttributeSet attrs) {
        super(context, attrs);
        initScreenSize((Activity)context);
        setFonts(context);
    }

    private void setFonts(Context context) {
        TextView child;
        FONT_NAME = Typeface.createFromAsset(context.getAssets(), "fonts/BadScript-Regular.ttf");
        for (int i=0; i<this.getChildCount(); i++) {
            child = (TextView) this.getChildAt(i);
            child.setTypeface(FONT_NAME);
            child.setTextColor(context.getResources().getColor(R.color.abc_primary_text_material_dark));
            child.setTextSize(width/20);
          //  child.setBackgroundColor(Color.parseColor("#33000000"));

        }


    }

    private void initScreenSize(Activity context) {
        width = context.getWindowManager().getDefaultDisplay().getWidth();
        height = context.getWindowManager().getDefaultDisplay().getHeight();

    }

}
