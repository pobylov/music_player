package com.pobylovskiy.artem.musicplayer.adapters.artist;

import android.content.Context;
import android.database.Cursor;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * Created by Artem on 08.09.2015.
 */
public class SimpleCursorAdapterForTrack extends SimpleCursorAdapter {

    public SimpleCursorAdapterForTrack(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    @Override
    public void setViewText(TextView v, String text) {

        try {
            Double.valueOf(text);
            v.setText(timeConversion(text));
        }catch (NumberFormatException e){
            v.setText(text);
        }

    }

    private String timeConversion(String miliSec) {


        long miliSeconds = Long.valueOf(miliSec);
        int MINUTES_IN_AN_HOUR = 60;
        int SECONDS_IN_A_MINUTE = 60;
        int MILISECONDS_IN_A_SECONDS = 1000;

        long seconds = miliSeconds / MILISECONDS_IN_A_SECONDS;

        long minutes = seconds / SECONDS_IN_A_MINUTE;
        seconds -= minutes * SECONDS_IN_A_MINUTE;

        long hours = minutes / MINUTES_IN_AN_HOUR;
        minutes -= hours * MINUTES_IN_AN_HOUR;

        if(seconds-10<0) {

            if (hours != 0) {
                return hours + ":" + minutes + ":" + "0"+seconds;
            } else {
                return minutes + ":" + "0"+seconds;
            }
        }
        else {

            if (hours != 0) {
                return hours + ":" + minutes + ":" + seconds;
            } else {
                return minutes + ":" + seconds;
            }
        }

    }
}
