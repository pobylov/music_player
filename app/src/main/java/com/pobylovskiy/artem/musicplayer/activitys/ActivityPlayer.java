package com.pobylovskiy.artem.musicplayer.activitys;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pobylovskiy.artem.musicplayer.BitmapActions;
import com.pobylovskiy.artem.musicplayer.Blur;
import com.pobylovskiy.artem.musicplayer.Constants;
import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.ScreenSizeProvider;
import com.pobylovskiy.artem.musicplayer.custom.CustomTextView;
import com.pobylovskiy.artem.musicplayer.custom.CustomTextViewForPlayer;
import com.pobylovskiy.artem.musicplayer.model.track.Track;
import com.pobylovskiy.artem.musicplayer.record_view.surface.CircleProgressSurface;

import java.util.ArrayList;


public class ActivityPlayer extends AppCompatActivity implements View.OnClickListener {


    // private ImageView image;
    private ImageView play, stop, next, prev, trackList;
    private AppBarLayout blurContainer;
    private Track track;
    private ArrayList<Track> tracks;
    private CustomTextView  artistName;
    private CustomTextViewForPlayer trackName;
    private boolean playToggle = false;
    private boolean resumeToggle = false;
    private TextView trackLength;
    private FrameLayout surfacePlayer;
    private CircleProgressSurface circleProgressSurface;
    private int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        blurContainer = (AppBarLayout) findViewById(R.id.blur_container);
        circleProgressSurface = (CircleProgressSurface) findViewById(R.id.circle_progress);
     //recordView = (RecordView) findViewById(R.id.record_view);
        play = (ImageView) findViewById(R.id.play);
        next = (ImageView) findViewById(R.id.next);
        prev = (ImageView) findViewById(R.id.prev);
        trackList = (ImageView) findViewById(R.id.iv_track_list);
        trackName = (CustomTextViewForPlayer) findViewById(R.id.player_track_name);
        artistName = (CustomTextView) findViewById(R.id.player_artist_name);
        surfacePlayer = (FrameLayout) findViewById(R.id.surface_container);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setLogo(getResources().getDrawable(R.drawable.logo));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        //pause = (Button) result.findViewById(R.id.pause);

        //surfacePlayer.addView(new CircleProgressSurface(this));


        track = getIntent().getParcelableExtra(Constants.ActivityPlayerArguments.TRACK);

        tracks = getIntent().getParcelableArrayListExtra(Constants.ActivityPlayerArguments.TRACK_LIST);
       // position = getPosition();
        circleProgressSurface.setCircleImage(getBitmap());
        circleProgressSurface.setTrackLength(getTrackLength());
        trackName.setText(getTrackName());
        artistName.setText(getArtistName());



        trackList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        for(int i=0; i<tracks.size(); i++){
          //  Toast.makeText(this, tracks.get(i).getName(), Toast.LENGTH_SHORT).show();
        }
        play.setOnClickListener(this);
        next.setOnClickListener(this);
        prev.setOnClickListener(this);

        // pause.setOnClickListener(this);
        //recordView.setRecordImage(getActivity().getResources().getDrawable(R.drawable.album));
       // recordView.setRecordAttr(getTrackLength(), getBitmap()== null ?  BitmapActions.drawableToBitmap(getResources().getDrawable(R.drawable.artwork)): getBitmap() );

    }

    private Bitmap getBitmap(){

        try {
            String albumCover = track.getAlbumId();
            return BitmapFactory.decodeFile(albumCover);
        }catch (NullPointerException e){

            return BitmapActions.drawableToBitmap(getResources().getDrawable(R.drawable.artwork));
        }
    }

  /*  private int getPosition(){
        return (int) extras.get(Constants.ActivityPlayerArguments.POSITION);
    }*/

    private long getTrackLength() {

        return Long.valueOf(track.getLength())/1000;
    }

    private String getTrackName(){

        return track.getName();
    }


    private String getArtistName(){

        return track.getArtist();
    }

    @Override
    public void onStart() {
        super.onStart();
        setBlurBackground();
        //trackName.scrollBy(trackName.getScrollX(), trackName.getScrollX()+ trackName.getWidth());
        //   blurContainer.setBackground(getActivity().getResources().getDrawable(R.drawable.fon));
    }

    private void setBlurBackground(){

        if(getBitmap() !=null && getBitmap() !=BitmapActions.drawableToBitmap(getResources().getDrawable(R.drawable.artwork))) {
            blur(getBitmap(), 60);
        }else {
            blurContainer.setBackground(getResources().getDrawable(R.drawable.fon21));
        }
    }

    private void blur(Bitmap bitmap, int radius){
        Bitmap blurImage = BitmapActions.bitmapResize(bitmap,
                (int) ScreenSizeProvider.getScreenWidth(this),
                (int) ScreenSizeProvider.getScreenHeight(this));
        blurContainer.setBackground(BitmapActions.bitmapToDrawable(Blur.fastblur(this, blurImage, radius)));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play:
                if(!resumeToggle) {
                    play.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_white_48dp));
                    resumeToggle = true;
                }else if(resumeToggle) {
                    play.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_arrow_white_48dp));
                    resumeToggle = false;
                }

                //play.setColorFilter(getActivity().getResources().getColor(R.color.primary), PorterDuff.Mode.MULTIPLY);
                break;
            case R.id.prev:
                break;
            case R.id.next:
                break;
        }

    }


}
