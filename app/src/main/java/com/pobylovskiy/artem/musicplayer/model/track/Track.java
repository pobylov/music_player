package com.pobylovskiy.artem.musicplayer.model.track;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Artem on 02.01.2016.
 */
public class Track implements Parcelable {

    String name;
    String artist;
    String length;


    int mode;
    String albumId;

    public Track(String name, String artist, String length, int mode) {
        this.name = name;
        this.artist = artist;
        this.length = length;
        this.mode = mode;
    }


    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getMode() {
        return mode;
    }

    public String getLength() {
        return length;
    }



    public String getArtist() {
        return artist;
    }

    public String getName() {
        return name;
    }


    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.artist);
        dest.writeString(this.length);
        dest.writeInt(this.mode);
        dest.writeString(this.albumId);
    }

    private Track(Parcel in) {
        this.name = in.readString();
        this.artist = in.readString();
        this.length = in.readString();
        this.mode = in.readInt();
        this.albumId = in.readString();
    }

    public static final Creator<Track> CREATOR = new Creator<Track>() {
        public Track createFromParcel(Parcel source) {
            return new Track(source);
        }

        public Track[] newArray(int size) {
            return new Track[size];
        }
    };
}
