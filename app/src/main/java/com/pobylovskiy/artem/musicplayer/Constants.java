package com.pobylovskiy.artem.musicplayer;

/**
 * Created by Artem on 14.08.2015.
 */
public class Constants {

    public static class ActivityPlayerArguments{
        public static final String TRACK= "TRACK";
        public static final String TRACK_LIST= "TRACK_LIST";
        public static final String ALBUM_COVER= "ALBUM_COVER";
        public static final String ALL_TRACK_NAME= "ALL_TRACK_NAME";
        public static final String TRACK_NAME= "TRACK_NAME";
        public static final String POSITION= "POSITION";
        public static final String TRACK_LENGTH= "TRACK_LENGTH";
    }

    public static class FlagsForDetailsItem{
        public static final String ID= "ID";
        public static final String WHERE = "WHERE";
        public static final String DETAILS= "DETAILS";
        public static final String PLAYLIST_NAME= "PLAYLIST_NAME";
    }

    public static class TypeForDetailsItem{
        public static final String ALBUM_DETAILS = "ALBUM_DETAILS";
        public static final String ARTIST = "ARTIST";
        public static final String PLAYLIST = "PLAYLIST";
    }

    public static class TypeForSearchInMediaStore{
        public static final String ALBUM_DATA= "ALBUM";
        public static final String ARTIST_DATA= "ARTIST";
        public static final String PLAYLIST_DATA= "PLAYLIST";
        public static final String TRACK_DATA= "TRACK";
    }

}
