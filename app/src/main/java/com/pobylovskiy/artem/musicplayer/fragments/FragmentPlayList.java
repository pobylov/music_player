package com.pobylovskiy.artem.musicplayer.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.pobylovskiy.artem.musicplayer.Constants;
import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.database.DBManager;
import com.pobylovskiy.artem.musicplayer.database.MyCursorLoader;

public class FragmentPlayList extends ListFragment implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor> {


    private SimpleCursorAdapter scAdapter;
    DBManager dbManager;

    public static final String KEY_POSITION = "position";

    public static FragmentPlayList newInstance(int position) {
        FragmentPlayList fragment = new FragmentPlayList();
        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        fragment.setArguments(args);
        return (fragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View result = inflater.inflate(R.layout.fragment_play_list, container, false);

        if(!getRetainInstance()){

            String[] from = new String[] {MediaStore.Audio.PlaylistsColumns.NAME};
            int[] to = new int[] { R.id.item_playlist_name};

            dbManager = DBManager.INSTANCE.setContext(getActivity());
            // создааем адаптер и настраиваем список
            scAdapter = new SimpleCursorAdapter(getActivity() , R.layout.list_item_playlist, null, from, to, 0);

            setListAdapter(scAdapter);

            getLoaderManager().initLoader(0, null, this);


        }

        setRetainInstance(true);
        return (result);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Cursor c = ((SimpleCursorAdapter) l.getAdapter()).getCursor();
        c.moveToPosition(position);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.FlagsForDetailsItem.ID, c.getString(c.getColumnIndex(
                MediaStore.Audio.Playlists._ID)));
        bundle.putString(Constants.FlagsForDetailsItem.WHERE ,MediaStore.Audio.Playlists.Members.PLAYLIST_ID);


        new BuilderFragmentTransaction()
                .fragmentManager(getFragmentManager())
                .fragment(new FragmentTrack())
                .bundle(bundle)
                .transact(R.id.container_for_track, true);
    }



    @Override
    public void onStart() {
        super.onStart();



    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), dbManager, Constants.TypeForSearchInMediaStore.PLAYLIST_DATA);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        scAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
