package com.pobylovskiy.artem.musicplayer.model.album;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Artem on 05.01.2016.
 */
public class Album implements Parcelable {

    private String name;
    private String quantity;
    private String cover;

    public Album(String name, String quantity, String cover) {
        this.name = name;
        this.quantity = quantity;
        this.cover = cover;
    }

    public String getCover() {
        return cover;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getName() {
        return name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.quantity);
        dest.writeString(this.cover);
    }

    private Album(Parcel in) {
        this.name = in.readString();
        this.quantity = in.readString();
        this.cover = in.readString();
    }

    public static final Parcelable.Creator<Album> CREATOR = new Parcelable.Creator<Album>() {
        public Album createFromParcel(Parcel source) {
            return new Album(source);
        }

        public Album[] newArray(int size) {
            return new Album[size];
        }
    };
}
