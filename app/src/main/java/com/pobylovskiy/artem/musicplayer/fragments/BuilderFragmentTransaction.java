package com.pobylovskiy.artem.musicplayer.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;


public class BuilderFragmentTransaction {

    private Fragment fragment;
    private FragmentManager fragmentManager;
    private Bundle bundle;


    public BuilderFragmentTransaction fragment(Fragment fragment){
       this.fragment = fragment;
        return this;
    }

    public BuilderFragmentTransaction fragmentManager(FragmentManager fragmentManager){
       this.fragmentManager = fragmentManager;
        return this;
    }

    public BuilderFragmentTransaction bundle(Bundle bundle){
        this.bundle = bundle;
        return this;
    }

    public void transact (int container, boolean addToBackStack){
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        if(addToBackStack) {
            if (fragmentManager != null && fragment != null) {
                fragmentManager.beginTransaction().replace(container, fragment).addToBackStack(null).commit();
            }
        }
        else {
            if (fragmentManager != null && fragment != null) {
                fragmentManager.beginTransaction().replace(container, fragment).commit();
            }
        }
    }


    public void detach (){
                   if (fragmentManager != null && fragment != null) {
                fragmentManager.beginTransaction().detach(fragment).commit();
            }

    }
}
