package com.pobylovskiy.artem.musicplayer.record_view.surface;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.TextView;

import com.pobylovskiy.artem.musicplayer.BitmapActions;
import com.pobylovskiy.artem.musicplayer.R;


public class DrawThreadProgress extends Thread implements ThreadStatus{


    private RectF rectBig = new RectF();
    private RectF rectSmall = new RectF();
    private Paint paintProgress,  paintCircle,  paintShadow, paintText, paintCover;
    private long trackLength = 1;
    private long counter = 1;

    private float width;
    private float height;
    private float progress=0;
    private SurfaceHolder surfaceHolder;
    private volatile Thread thread;
    private volatile boolean threadSuspended = false;
    private Context activity;
    private int time=1000;


    private Bitmap cover;
    private Drawable drawable;
    private float reSizeFactor = 1.84f;

    public DrawThreadProgress(SurfaceHolder surfaceHolder,Context activity ,float width, float height){
        this.surfaceHolder = surfaceHolder;
        this.width = width;
        this.height = height;
        this.activity = activity;
        initPaint();
        thread = new Thread(this);
    }

    private void initPaint() {
        paintProgress = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintProgress.setColor(Color.parseColor("#E91E63"));
        paintProgress.setStyle(Paint.Style.STROKE);

        paintShadow = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintShadow.setColor(Color.DKGRAY);
        paintShadow.setStyle(Paint.Style.FILL);
        paintShadow.setAlpha(90);

        paintCircle = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintCircle.setColor(Color.parseColor("#E91E63"));
        paintCircle.setStyle(Paint.Style.FILL);

        Typeface tf =Typeface.createFromAsset(activity.getAssets(),
                "fonts/IntroRustBook-Base.otf");
        paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintText.setColor(Color.WHITE);
        paintText.setTypeface(tf);

        paintCover = new Paint(Paint.ANTI_ALIAS_FLAG);


    }

    private void createCircleImage(){
        try {
            drawable = new CircleImage(BitmapActions.bitmapResize(
                    cover, (int) (width / reSizeFactor), (int) (width / reSizeFactor)));
        }catch (Exception e){
            Bitmap bitmap = BitmapActions.drawableToBitmap(activity.getResources().getDrawable(R.drawable.artwork));
            drawable = new CircleImage(BitmapActions.bitmapResize(
                    bitmap, (int) (width / reSizeFactor), (int) (width / reSizeFactor)));
        }
    }

    @Override
    public void onStart(){
        thread.start();
    }

    @Override
    public synchronized void onStop() {

        Thread moribund = thread;
        thread = null;
        moribund.interrupt();

        Log.d("Threadstatus", "stop");
    }

    @Override
    public void onPause(){
        synchronized (this) {
            threadSuspended = true;
            Log.d("Threadstatus", "pause");
        }
    }

    @Override
    public void onResume() {
        synchronized (this) {
            threadSuspended = false;
            Log.d("Threadstatus", "resume");
            notify();
        }
    }

    public void setSleepTime(int time){
        this.time = time;
    }

    @Override
    public void run() {
        Canvas canvas;
        createCircleImage();
        Thread thisThread = Thread.currentThread();
        for (float i = 0; i <= trackLength; i++) {

            if (thread == thisThread) {
                try {
                    Thread.sleep(time);
                    setProgress(i);

                    synchronized (this) {
                        while (threadSuspended) {
                            wait();
                        }
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                try{
                canvas = null;
                update(canvas);
                }catch (Exception e){}


            }
        }
    }


    private void update(Canvas canvas){
        try {
            // получаем объект Canvas и выполняем отрисовку
            canvas = surfaceHolder.lockCanvas(null);
            synchronized (surfaceHolder) {
                draw(canvas);
            }
        } finally {
            if (canvas != null) {
                // отрисовка выполнена. выводим результат на экран
                surfaceHolder.unlockCanvasAndPost(canvas);

            }
        }
    }

    private void draw(Canvas canvas){
        canvas.drawColor(0, android.graphics.PorterDuff.Mode.CLEAR);

        canvas.drawBitmap(BitmapActions.drawableToBitmap(drawable), width/2-width/3+width/15, height/2-width/3+width/15, paintCover);


        paintProgress.setStrokeWidth(width/80);
        paintShadow.setStrokeWidth(width/90);
        paintText.setTextSize(width/15);

        rectBig.set(width/2-width/3+width/15, height/2-width/3+width/15 , width/2+width/3-width/15, height/2+width/3-width/15);

        float radiusImage= width/3-width/15;//((width/2+width/3) - (width/2-width/3))/2 - width/15;
        float angle = 360 * (progress)/ trackLength;
        //Log.d("SPHERA->DECART", String.valueOf(radiusImage * Math.cos(angle + 80)) + " " + String.valueOf(radiusImage * Math.sin(angle + 80)));

        float textWidth = paintText.measureText(timeConversion(counter));
        paintShadow.setAlpha(40);
        canvas.drawText(timeConversion(counter--), width/2-textWidth/2, width/15, paintText);


        canvas.drawArc(rectBig, 90f, angle, false, paintProgress);
        canvas.drawCircle((float) (radiusImage* Math.cos(Math.toRadians(angle + 90)) + (width / 2)), (float) (radiusImage * Math.sin(Math.toRadians(angle + 90)) + (height / 2)), 20f, paintShadow);
        canvas.drawCircle((float) (radiusImage * Math.cos(Math.toRadians(angle + 90)) + (width / 2)), (float) (radiusImage * Math.sin(Math.toRadians(angle + 90)) + (height / 2)), 15f, paintCircle);

    }

    private String timeConversion(long sec) {


        long seconds = Long.valueOf(sec);
        int MINUTES_IN_AN_HOUR = 60;
        int SECONDS_IN_A_MINUTE = 60;
        int MILISECONDS_IN_A_SECONDS = 1000;


        long minutes = seconds / SECONDS_IN_A_MINUTE;
        seconds -= minutes * SECONDS_IN_A_MINUTE;

        long hours = minutes / MINUTES_IN_AN_HOUR;
        minutes -= hours * MINUTES_IN_AN_HOUR;

        if(seconds-10<0) {

            if (hours != 0) {
                return hours + ":" + minutes + ":" + "0"+seconds;
            } else {
                return minutes + ":" + "0"+seconds;
            }
        }
        else {

            if (hours != 0) {
                return hours + ":" + minutes + ":" + seconds;
            } else {
                return minutes + ":" + seconds;
            }
        }

    }



    public void setCover(Bitmap cover) {
        this.cover = cover;
    }
    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;

    }
    public long getTrackLength() {
        return trackLength;
    }

    public void setTrackLength(long trackLength) {
        this.trackLength = trackLength;
        counter = trackLength;
    }
}