package com.pobylovskiy.artem.musicplayer;

import android.app.Activity;
import android.content.Context;

import java.security.AccessControlContext;

public class ScreenSizeProvider {

    public static float getScreenWidth(Activity activity){
        return activity.getWindowManager().getDefaultDisplay().getWidth();
    }

    public static float getScreenHeight(Activity activity){
        return activity.getWindowManager().getDefaultDisplay().getHeight();
    }

}
