package com.pobylovskiy.artem.musicplayer.activitys;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.pobylovskiy.artem.musicplayer.R;
import com.pobylovskiy.artem.musicplayer.fragments.BuilderFragmentTransaction;
import com.pobylovskiy.artem.musicplayer.fragments.FragmentAlbumList;
import com.pobylovskiy.artem.musicplayer.fragments.FragmentArtistList;
import com.pobylovskiy.artem.musicplayer.fragments.FragmentBoot;
import com.pobylovskiy.artem.musicplayer.fragments.FragmentPlayList;
import com.pobylovskiy.artem.musicplayer.fragments.FragmentTrackList;
import com.pobylovskiy.artem.musicplayer.page_dot_controller.DotController;
import com.pobylovskiy.artem.musicplayer.view_pager.PagerTitleStrip;
import com.pobylovskiy.artem.musicplayer.view_pager.SampleAdapter;
import com.pobylovskiy.artem.musicplayer.view_pager.ZoomOutPageTransformer;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private ViewPager generalPager;
    private PagerTitleStrip pagerTitleStrip;
    private FrameLayout dotContainer, playerContainer;
    private DotController dotController;
    private FragmentManager fragmentManager;
    private FragmentBoot fragmentBoot;
    private ViewGroup boot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentBoot = FragmentBoot.newInstance();
       /* new BuilderFragmentTransaction().fragment(fragmentBoot).fragmentManager(getSupportFragmentManager()).transact(R.id.fragment_player_container, false);
        */new bootThread().execute();
        generalPager = (ViewPager) findViewById(R.id.general_pager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs_for_graph);
        playerContainer = (FrameLayout) findViewById(R.id.fragment_player_container);
        boot = (ViewGroup) findViewById(R.id.fr_boot);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setLogo(getResources().getDrawable(R.drawable.logo));
       // dotContainer = (FrameLayout) findViewById(R.id.dot_container);
      // pagerTitleStrip = (PagerTitleStrip) findViewById(R.id.pager_title_strip);
       // playerContainer = (FrameLayout) findViewById(R.id.fragment_player_container);

        generalPager.setAdapter(new SampleAdapter(getSupportFragmentManager(), this));
        generalPager.setPageTransformer(true, new ZoomOutPageTransformer());
       // generalPager.setOffscreenPageLimit(3);

        setupViewPager(generalPager);
        tabLayout.setupWithViewPager(generalPager);
        generalPager.setVisibility(View.INVISIBLE);

    }


    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(FragmentAlbumList.newInstance(1), "Album");
        adapter.addFragment(FragmentPlayList.newInstance(2), "Playlist");
        adapter.addFragment(FragmentTrackList.newInstance(3), "Track");
        adapter.addFragment(FragmentArtistList.newInstance(4), "Artist");
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



    class bootThread extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
           // //playerContainer.removeAllViews();

            generalPager.setVisibility(View.VISIBLE);
            boot.setVisibility(View.GONE);
        }
    }
}
